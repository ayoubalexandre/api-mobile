Feature: testar API permissão geolocation

  Scenario Outline: Solicitar permissão de uso da localização
    When eu passar a "<funcaoApi>"
    Then o sistema deve retornar o "<status>"
    Examples:
      | funcaoApi               | status |
      | /periferico/geolocation | 200    |