package cucumberTest.steps;

import appium.core.AndroidDriverFactory;
import io.appium.java_client.android.AndroidDriver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;

import static io.restassured.RestAssured.*;

public class ApiTeste {

    private AndroidDriver driver;
    private String api;

    @When("eu passar a {string}")
    public void eu_passar_a(String funcaoApi) {
        //chamar a api
        driver = AndroidDriverFactory.getAndroidDriver();
        this.api = funcaoApi;
    }

    @Then("o sistema deve retornar o {string}")
    public void o_sistema_deve_retornar_o(String status) {

        baseURI = "URL";
        RequestSpecification httpRequest = given();
        Response response = httpRequest.get();
        // Access header with a given name.
        String amcSessionId = response.getHeader("amc-session-id");
        System.out.println("amc-session-id: " + amcSessionId);


        given()
                .when()
                .header("amc-session-id", amcSessionId)
                .get("http://192.168.0.8:8080" + api)
                .then()
                .statusCode(Matchers.is(Integer.valueOf(status)));
        driver.quit();
    }
}
