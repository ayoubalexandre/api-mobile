package appium.core;

import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class IosDriverFactory {

    private static IOSDriver iosDriver;

    public static IOSDriver getIosDriver(){
        if(iosDriver == null){
            createIosDriver();
        }
        return iosDriver;
    }
    private static void createIosDriver(){
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability("platformName", "iOS");
        desiredCapabilities.setCapability("platformVersion","15.5");
        desiredCapabilities.setCapability("appium:deviceName", "iPhone SE");
        desiredCapabilities.setCapability("appium:automationName", "XCUITest");
        desiredCapabilities.setCapability("appium:app", "C:\\Users\\Alex\\Desktop\\ipa_teste_api\\IPA");

        try {
            iosDriver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), desiredCapabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static void killIosDriver(){
        if (iosDriver != null){
            iosDriver.quit();
            iosDriver = null;
        }
    }
}
