package dsl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static appium.core.IosDriverFactory.*;

public class IosDSL {

    public void escrever(By by, String texto){
        getIosDriver().findElement(by)
                .sendKeys(texto);
    }

    public void clicar(By by){
        getIosDriver().findElement(by)
                .click();
    }

    public void clicarPorTexto(By by, String texto){
        getIosDriver().findElement(By.xpath("//*[@text='"+texto+"']")).click();
    }

    public void limpar(By by){
        getIosDriver().findElement(by)
                .clear();
    }

    public String obterTexto(By by){
        return getIosDriver().findElement(by).getText();
    }

    public void selecionarCombo(By by, String valor){
        getIosDriver().findElement(by).click();
        clicarTexto(valor);
    }

    public void clicarTexto(String texto){
        getIosDriver().findElement(By.xpath("//*[@text='"+texto+"']")).click();
    }

    public boolean isCheckMarcardo(By by){
        return getIosDriver().findElement(by).getAttribute("checked").equals("true");
    }

    public void esperaImplicita(){
        getIosDriver().manage().timeouts().getImplicitWaitTimeout();
    }

    public void esperaExplicita(String xpath){
        WebDriverWait wait = new WebDriverWait(getIosDriver(), 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
    }

    public void singleTap(int x, int y){
        new TouchActions(getIosDriver()).singleTap(x,y).perform();
    }

    public void doubleTap(int x, int y){
        new TouchActions(getIosDriver()).doubleTap(x,y).perform();
    }

    public void clicarSeekBar(By by, double posicao, int delta){
        //delta é a diferença entre a posição inicial do quadro e o componente
        //esse delta é utilizado para aproximar o cálculo do valor desejado para o componente seek

        WebElement seek = getIosDriver().findElement(by);
        int y = seek.getLocation().y + (seek.getSize().height / 2);
        int xinicial = seek.getLocation().x + delta;
        int x = (int) (xinicial + ((seek.getSize().width -2*delta) * posicao));

        singleTap(x, y);
    }

    

}
